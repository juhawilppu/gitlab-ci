#!/bin/bash

waited=0
until [ "`curl --silent --show-error --connect-timeout 1 -I http://docker:5000 | grep '302'`" != "" ];
do
  echo $waited s - sleeping for 10 seconds
  docker logs app_backend_1 --since=10s
  waited=$(($waited+10))
  sleep 10
  if [[ $waited -ge 300 ]];
   then
      echo "Failed to start in 5 minutes"
      exit 1
   fi
done

echo "Application is ready"