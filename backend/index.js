const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const Sequelize = require('sequelize');
const cookieSession = require('cookie-session');
const keys = require('./config/keys');
const fs = require('fs');

const sequelize = new Sequelize(keys.postgres.database, keys.postgres.username, keys.postgres.password, {
    host: keys.postgres.host,
    dialect: 'postgresql',
    define: {
        timestamps: true,
        paranoid: true,
        underscored: true
    },
    force: true // This will DROP tables and rebuild schema
});

app.use(bodyParser.json());

if (process.env.NODE_ENV === 'production') {
    // Express will serve the client main.js etc.
    app.use(express.static('client/build'));

    // Express will redirect to / if it's doesn't recognize the route
    const path = require('path');
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}

const PORT = process.env.PORT || 5000;

app.listen(PORT);
