## Dockerfile used for running the backend NodeJS application with embedded build of frontend.
## Essentially this is the production Docker image

FROM node:alpine
COPY backend .
RUN npm start
